# ModernCV académique

Un template pour faire de jolis cv académiques avec une classe LaTeX moderncv légèrement modifiée. Requiert les paquets (contenus dans Texlive2019)
- moderncv
- moderntimeline
- graphicx

![Alt-Text](<./example.png>)
